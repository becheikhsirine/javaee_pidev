package ManagedBean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="joinForumBean")
@SessionScoped
public class JoinForumBean implements Serializable {

	public JoinForumBean() {
		// TODO Auto-generated constructor stub
	}
	public String doJoin() {
		String navigateTo="null";
		navigateTo= "/pages/subject/addSubject?faces-redirect=true";
		return navigateTo;

	}

}
